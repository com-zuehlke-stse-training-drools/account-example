package com.zuehlke.stse.education.drools.example.banking;

import com.zuehlke.stse.education.drools.example.banking.model.domain.Account;
import com.zuehlke.stse.education.drools.example.banking.model.domain.AccountPeriod;
import com.zuehlke.stse.education.drools.example.banking.model.domain.CashFlow;
import org.junit.Test;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

import static org.junit.Assert.assertTrue;

public class KnowledgeBaseTest {
    
    @Test
    public void execute() {
        KieServices ks = KieServices.Factory.get();
        KieContainer kContainer = ks.getKieClasspathContainer();
    
        KieSession kSession = null;
        try {
            kSession = kContainer.newKieSession();
    
            final Account a1 = new Account(1, BigDecimal.valueOf(0));
            final Account a2 = new Account(2, BigDecimal.valueOf(0));
            final Account a3 = new Account(3, BigDecimal.valueOf(0));
    
            final CashFlow c1 = new CashFlow(Instant.parse("2016-11-23T10:00:00Z"), a2, a1, BigDecimal.valueOf(20));
            final CashFlow c2 = new CashFlow(Instant.parse("2016-11-23T09:00:00Z"), a2, a3, BigDecimal.valueOf(35));
            final CashFlow c3 = new CashFlow(Instant.parse("2016-12-23T14:20:00Z"), a1, a3, BigDecimal.valueOf(100));
    
            final AccountPeriod ap = new AccountPeriod(Instant.parse("2016-11-01T00:00:00Z"), Instant.parse("2016-12-01T00:00:00Z"));
            
            kSession.insert(a1);
            kSession.insert(a2);
            kSession.insert(a3);
    
            kSession.insert(c1);
            kSession.insert(c2);
            kSession.insert(c3);
    
            kSession.insert(ap);
            kSession.fireAllRules();
    
            assertTrue(a1.getBalance().compareTo(BigDecimal.valueOf(20)) == 0);
            assertTrue(a2.getBalance().compareTo(BigDecimal.valueOf(-55)) == 0);
            assertTrue(a3.getBalance().compareTo(BigDecimal.valueOf(35)) == 0);
        } finally {
            if (!Objects.isNull(kSession)) {
                kSession.dispose();
            }
        }
    }
}
