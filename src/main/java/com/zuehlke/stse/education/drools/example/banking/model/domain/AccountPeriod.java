package com.zuehlke.stse.education.drools.example.banking.model.domain;

import java.time.Instant;

public class AccountPeriod {
    private final Instant lowerBound;
    private final Instant upperBound;
    
    public AccountPeriod(final Instant lowerBound, final Instant upperBound) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }
    
    public Instant getLowerBound() {
        return lowerBound;
    }
    
    public Instant getUpperBound() {
        return upperBound;
    }
    
    public boolean overlaps(Instant timestamp) {
        return (timestamp.isAfter(lowerBound) || timestamp.equals(lowerBound)) && (timestamp.isBefore(upperBound));
    }
    
    @Override
    public String toString() {
        return new StringBuilder()
                .append("AccountPeriod")
                .append("[")
                .append("lowerBound = ")
                .append(this.getLowerBound())
                .append(", ")
                .append("upperBound = ")
                .append(this.getUpperBound())
                .append("]")
                .toString();
    }
}
