package com.zuehlke.stse.education.drools.example.banking.model.domain;

import org.kie.api.definition.type.Modifies;
import org.kie.api.definition.type.PropertyReactive;

import java.math.BigDecimal;
import java.util.Objects;

@PropertyReactive
public class Account {
    private final long id;
    private BigDecimal balance;
    
    public Account(final long id) {
        this(id, BigDecimal.ZERO);
    }
    
    public Account(final long id, final BigDecimal balance) {
        this.id = id;
        this.balance = Objects.requireNonNull(balance);
    }
    
    @Modifies({ "balance" })
    public void credit(BigDecimal amount) {
        balance = balance.add(amount);
    }
    
    @Modifies({ "balance" })
    public void debit(BigDecimal amount) {
        balance = balance.subtract(amount);
    }
    
    public BigDecimal getBalance() {
        return balance;
    }
    
    public long getId() {
        return id;
    }
    
    @Override
    public String toString() {
        return new StringBuilder()
                .append("Account")
                .append("[")
                .append("id = ")
                .append(this.getId())
                .append(", ")
                .append("balance = ")
                .append(this.getBalance())
                .append("]")
                .toString();
    }
}
