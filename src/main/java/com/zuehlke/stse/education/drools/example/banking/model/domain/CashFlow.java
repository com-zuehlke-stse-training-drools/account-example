package com.zuehlke.stse.education.drools.example.banking.model.domain;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

public class CashFlow {
    private final Instant timestamp;
    private final BigDecimal amount;
    private final Account source;
    private final Account target;
    
    public CashFlow(final Instant timestamp, Account source, Account target, final BigDecimal amount) {
        this.timestamp = Objects.requireNonNull(timestamp);
        this.source = Objects.requireNonNull(source);
        this.target = Objects.requireNonNull(target);
        this.amount = Objects.requireNonNull(amount);
    }
    
    public Instant getTimestamp() {
        return timestamp;
    }
    
    public BigDecimal getAmount() {
        return amount;
    }
    
    public Account getSource() {
        return source;
    }
    
    public Account getTarget() {
        return target;
    }
    
    @Override
    public String toString() {
        return new StringBuilder()
                .append("CashFlow")
                .append("[")
                .append("timestamp = ")
                .append(this.getTimestamp())
                .append(", ")
                .append("source = ")
                .append(this.getSource())
                .append(", ")
                .append("target = ")
                .append(this.getTarget())
                .append(", ")
                .append("amount = ")
                .append(this.getAmount())
                .append("]")
                .toString();
    }
}
